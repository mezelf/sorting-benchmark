import create_array
import python_sorter_int
import python_sorter_string
import os.path,subprocess
from subprocess import STDOUT,PIPE

def compile_java(java_file):
    subprocess.check_call(['javac', java_file])

def execute_java(java_file, stdin):
    java_class,ext = os.path.splitext(java_file)
    cmd = ['java', java_class]
    proc = subprocess.Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=STDOUT)
    stdout,stderr = proc.communicate(stdin)
    print ('This was "' + stdout + '"')

with open("scores.csv", 'r+') as f:
    f.truncate(0)
f = open("scores.csv", "a")
f.write("taal,bubblesort,selectionsort,instertionsort,mergesort,quicksort,heapsort,shellsort,buildinsort \n")
#intsorters
python_sorter_int()
compile_java('java_sorter_int.java')
execute_java('java_sorter_int.java', 'Jon')
#stringsorters
python_sorter_string()
