import numpy as np
import time


# sorting algoritmes are from https://www.programiz.com/

def bubbleSort(array):
    
  # loop to access each array element
  for i in range(len(array)):

    # loop to compare array elements
    for j in range(0, len(array) - i - 1):

      # compare two adjacent elements
      # change > to < to sort in descending order
      if array[j] > array[j + 1]:

        # swapping elements if elements
        # are not in the intended order
        temp = array[j]
        array[j] = array[j+1]
        array[j+1] = temp

def selectionSort(array):
   size = len(array)
   for step in range(size):
        min_idx = step

        for i in range(step + 1, size):
         
            # to sort in descending order, change > to < in this line
            # select the minimum element in each loop
            if array[i] < array[min_idx]:
                min_idx = i
         
        # put min at the correct position
        (array[step], array[min_idx]) = (array[min_idx], array[step])

def insertionSort(array):

    for step in range(1, len(array)):
        key = array[step]
        j = step - 1
        
        # Compare key with each element on the left of it until an element smaller than it is found
        # For descending order, change key<array[j] to key>array[j].        
        while j >= 0 and key < array[j]:
            array[j + 1] = array[j]
            j = j - 1
        
        # Place key at after the element just smaller than it.
        array[j + 1] = key
def mergeSort(array):
    if len(array) > 1:

        #  r is the point where the array is divided into two subarrays
        r = len(array)//2
        L = array[:r]
        M = array[r:]

        # Sort the two halves
        mergeSort(L)
        mergeSort(M)

        i = j = k = 0

        # Until we reach either end of either L or M, pick larger among
        # elements L and M and place them in the correct position at A[p..r]
        while i < len(L) and j < len(M):
            if L[i] < M[j]:
                array[k] = L[i]
                i += 1
            else:
                array[k] = M[j]
                j += 1
            k += 1

        # When we run out of elements in either L or M,
        # pick up the remaining elements and put in A[p..r]
        while i < len(L):
            array[k] = L[i]
            i += 1
            k += 1

        while j < len(M):
            array[k] = M[j]
            j += 1
            k += 1
def partition(array, low, high):

  # choose the rightmost element as pivot
  pivot = array[high]

  # pointer for greater element
  i = low - 1

  # traverse through all elements
  # compare each element with pivot
  for j in range(low, high):
    if array[j] <= pivot:
      # if element smaller than pivot is found
      # swap it with the greater element pointed by i
      i = i + 1

      # swapping element at i with element at j
      (array[i], array[j]) = (array[j], array[i])

  # swap the pivot element with the greater element specified by i
  (array[i + 1], array[high]) = (array[high], array[i + 1])

  # return the position from where partition is done
  return i + 1

# function to perform quicksort
def quickSort(array, low, high):
  if low < high:

    # find pivot element such that
    # element smaller than pivot are on the left
    # element greater than pivot are on the right
    pi = partition(array, low, high)

    # recursive call on the left of pivot
    quickSort(array, low, pi - 1)

    # recursive call on the right of pivot
    quickSort(array, pi + 1, high)

def heapify(arr, n, i):
    # Find largest among root and children
    largest = i
    l = 2 * i + 1
    r = 2 * i + 2

    if l < n and arr[i] < arr[l]:
        largest = l

    if r < n and arr[largest] < arr[r]:
        largest = r

    # If root is not largest, swap with largest and continue heapifying
    if largest != i:
        arr[i], arr[largest] = arr[largest], arr[i]
        heapify(arr, n, largest)

  
def heapSort(arr):
    n = len(arr)

    # Build max heap
    for i in range(n//2, -1, -1):
        heapify(arr, n, i)

    for i in range(n-1, 0, -1):
        # Swap
        arr[i], arr[0] = arr[0], arr[i]

        # Heapify root element
        heapify(arr, i, 0)

def shellSort(array):
    n = len(array)
    # Rearrange elements at each n/2, n/4, n/8, ... intervals
    interval = n // 2
    while interval > 0:
        for i in range(interval, n):
            temp = array[i]
            j = i
            while j >= interval and array[j - interval] > temp:
                array[j] = array[j - interval]
                j -= interval

            array[j] = temp
        interval //= 2
f = open("scores.csv", "a")
bubblescore =0
for x in range (10):

  randnums = np.loadtxt('int_array')
  start = time.time()
  bubbleSort(randnums)
  pass
  end = time.time()
  delta = (end - start) * 1000
  bubblescore += delta
print ("took %.2f seconds to bubblesort" % bubblescore)
f.write("python-int," + str(bubblescore )+ ",")


selectionscore = 0
for x in range (10):
  randnums = np.loadtxt('int_array')
  start = time.time()
  selectionSort(randnums)
  pass
  end = time.time()
  delta = (end - start) * 1000
  selectionscore += delta
print ("took %.2f seconds to selectionSort" % selectionscore)
f.write(str(selectionscore) + ",")

insertionscore = 0
for x in range (10):
  randnums = np.loadtxt('int_array')
  start = time.time()
  insertionSort(randnums)
  pass
  end = time.time()
  delta = (end - start) * 1000
  insertionscore += delta
print ("took %.2f seconds to insertionSort" % insertionscore)
f.write(str(insertionscore) + ",")


mergescore = 0
for x in range (10):
  randnums = np.loadtxt('int_array')
  start = time.time()
  mergeSort(randnums)
  pass
  end = time.time()
  delta = (end - start) * 1000
  mergescore += delta
print ("took %.2f seconds to mergeSort" % mergescore)
f.write(str(mergescore) + ",")

quickscore = 0
for x in range (10):
  randnums = np.loadtxt('int_array')
  start = time.time()
  size = len(randnums)
  quickSort(randnums,0,size-1)
  pass
  end = time.time()
  delta = (end - start) * 1000
  quickscore += delta
print ("took %.2f seconds to quickSort" % quickscore)
f.write(str(quickscore) + ",")

heapscore = 0
for x in range (10):
  randnums = np.loadtxt('int_array')
  start = time.time()
  heapSort(randnums)
  pass
  end = time.time()
  delta = (end - start) * 1000
  heapscore += delta
print ("took %.2f seconds to heapSort" % heapscore)
f.write(str(heapscore) + ",")

shellscore = 0
for x in range (10):
  randnums = np.loadtxt('int_array')
  start = time.time()
  shellSort(randnums)
  pass
  end = time.time()
  delta = (end - start) * 1000
  shellscore += delta
print ("took %.2f seconds to shellSort" % shellscore)
f.write(str(shellscore) + ",")

buidlinscore = 0
for x in range (10):
  randnums = np.loadtxt('int_array')
  start = time.time()
  randnums.sort()
  pass
  end = time.time()
  delta = (end - start) * 1000
  buidlinscore += delta
print ("took %.2f seconds to BuildinSort" % buidlinscore)
f.write(str(buidlinscore) + ", \n")