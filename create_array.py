import random
import numpy as np
import string

def get_random_string(length):
    # choose from all lowercase letter
    letters = string.ascii_lowercase
    result_str = ''.join(random.choice(letters) for i in range(length))
    return result_str

lengte = 1000
randnums= np.random.randint(1,10001,lengte)
np.savetxt('int_array', randnums, delimiter=',',fmt='%i')
randstring =[]
for x in range (lengte):
    randstring.append(get_random_string(random.randrange(1, 12, 1)))
np.savetxt('string_array', randstring, delimiter=" ", fmt="%s") 


