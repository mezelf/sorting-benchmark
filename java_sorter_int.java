import java.io.*;

class java_sorter_int {
    public static void main(String[] args) {
        try {
            File scores = new File("scores.csv");
            FileWriter fw = new FileWriter(scores, true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write("java-int,");
            int[] randomnumbers = support.getnumbers();
            long startTime = System.nanoTime();
            bubblesort.bubbleSort(randomnumbers);
            long endTime = System.nanoTime();
            float duration = (endTime - startTime) / 1000000f;
            bw.write(duration + ",");

            randomnumbers = support.getnumbers();
            startTime = System.nanoTime();
            SelectionSort.selectionSort(randomnumbers);
            endTime = System.nanoTime();
            duration = (endTime - startTime) / 1000000f;
            bw.write(duration + ",");

            randomnumbers = support.getnumbers();
            startTime = System.nanoTime();
            InsertionSort.insertionSort(randomnumbers);
            endTime = System.nanoTime();
            duration = (endTime - startTime) / 1000000f;
            bw.write(duration + ",");

            randomnumbers = support.getnumbers();
            startTime = System.nanoTime();
            MergeSort.mergeSort(randomnumbers, 0, randomnumbers.length - 1);
            endTime = System.nanoTime();
            duration = (endTime - startTime) / 1000000f;
            bw.write(duration + ",");

            randomnumbers = support.getnumbers();
            startTime = System.nanoTime();
            Quicksort.quickSort(randomnumbers, 0, randomnumbers.length - 1);
            endTime = System.nanoTime();
            duration = (endTime - startTime) / 1000000f;
            bw.write(duration + ",");

            randomnumbers = support.getnumbers();
            startTime = System.nanoTime();
            HeapSort.heapSort(randomnumbers);
            endTime = System.nanoTime();
            duration = (endTime - startTime) / 1000000f;
            bw.write(duration + ",");

            randomnumbers = support.getnumbers();
            startTime = System.nanoTime();
            ShellSort.shellSort(randomnumbers, randomnumbers.length);
            endTime = System.nanoTime();
            duration = (endTime - startTime) / 1000000f;
            bw.write(duration + ",");
            bw.newLine();
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

class support {
    static int[] getnumbers() {
        int[] randomnumbers = new int[1000];
        try {
            File file = new File("int_array"); // creates a new file instance
            FileReader fr = new FileReader(file); // reads the file
            BufferedReader br = new BufferedReader(fr); // creates a buffering character input stream
            String line;
            int i = 0;
            while ((line = br.readLine()) != null) {
                randomnumbers[i] = Integer.parseInt(line);
                i++;
            }
            fr.close(); // closes the stream and release the resources
        } catch (IOException e) {
            e.printStackTrace();
        }
        return randomnumbers;

    }
}

class bubblesort {
    // perform the bubble sort
    static void bubbleSort(int array[]) {
        int size = array.length;

        // loop to access each array element
        for (int i = 0; i < size - 1; i++)

            // loop to compare array elements
            for (int j = 0; j < size - i - 1; j++)

                // compare two adjacent elements
                // change > to < to sort in descending order
                if (array[j] > array[j + 1]) {

                    // swapping occurs if elements
                    // are not in the intended order
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
    }
}

class SelectionSort {
    static void selectionSort(int array[]) {
        int size = array.length;

        for (int step = 0; step < size - 1; step++) {
            int min_idx = step;

            for (int i = step + 1; i < size; i++) {

                // To sort in descending order, change > to < in this line.
                // Select the minimum element in each loop.
                if (array[i] < array[min_idx]) {
                    min_idx = i;
                }
            }

            // put min at the correct position
            int temp = array[step];
            array[step] = array[min_idx];
            array[min_idx] = temp;
        }
    }
}

class InsertionSort {

    static void insertionSort(int array[]) {
        int size = array.length;

        for (int step = 1; step < size; step++) {
            int key = array[step];
            int j = step - 1;

            // Compare key with each element on the left of it until an element smaller than
            // it is found.
            // For descending order, change key<array[j] to key>array[j].
            while (j >= 0 && key < array[j]) {
                array[j + 1] = array[j];
                --j;
            }

            // Place key at after the element just smaller than it.
            array[j + 1] = key;
        }
    }
}

class MergeSort {

    // Merge two subarrays L and M into arr
    static void merge(int arr[], int p, int q, int r) {

        // Create L ← A[p..q] and M ← A[q+1..r]
        int n1 = q - p + 1;
        int n2 = r - q;

        int L[] = new int[n1];
        int M[] = new int[n2];

        for (int i = 0; i < n1; i++)
            L[i] = arr[p + i];
        for (int j = 0; j < n2; j++)
            M[j] = arr[q + 1 + j];

        // Maintain current index of sub-arrays and main array
        int i, j, k;
        i = 0;
        j = 0;
        k = p;

        // Until we reach either end of either L or M, pick larger among
        // elements L and M and place them in the correct position at A[p..r]
        while (i < n1 && j < n2) {
            if (L[i] <= M[j]) {
                arr[k] = L[i];
                i++;
            } else {
                arr[k] = M[j];
                j++;
            }
            k++;
        }

        // When we run out of elements in either L or M,
        // pick up the remaining elements and put in A[p..r]
        while (i < n1) {
            arr[k] = L[i];
            i++;
            k++;
        }

        while (j < n2) {
            arr[k] = M[j];
            j++;
            k++;
        }
    }

    // Divide the array into two subarrays, sort them and merge them
    static void mergeSort(int arr[], int l, int r) {
        if (l < r) {

            // m is the point where the array is divided into two subarrays
            int m = (l + r) / 2;

            mergeSort(arr, l, m);
            mergeSort(arr, m + 1, r);

            // Merge the sorted subarrays
            merge(arr, l, m, r);
        }
    }
}

class Quicksort {

    // method to find the partition position
    static int partition(int array[], int low, int high) {

        // choose the rightmost element as pivot
        int pivot = array[high];

        // pointer for greater element
        int i = (low - 1);

        // traverse through all elements
        // compare each element with pivot
        for (int j = low; j < high; j++) {
            if (array[j] <= pivot) {

                // if element smaller than pivot is found
                // swap it with the greatr element pointed by i
                i++;

                // swapping element at i with element at j
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
            }

        }

        // swapt the pivot element with the greater element specified by i
        int temp = array[i + 1];
        array[i + 1] = array[high];
        array[high] = temp;

        // return the position from where partition is done
        return (i + 1);
    }

    static void quickSort(int array[], int low, int high) {
        if (low < high) {

            // find pivot element such that
            // elements smaller than pivot are on the left
            // elements greater than pivot are on the right
            int pi = partition(array, low, high);

            // recursive call on the left of pivot
            quickSort(array, low, pi - 1);

            // recursive call on the right of pivot
            quickSort(array, pi + 1, high);
        }
    }
}

class HeapSort {

    static public void heapSort(int arr[]) {
        int n = arr.length;

        // Build max heap
        for (int i = n / 2 - 1; i >= 0; i--) {
            heapify(arr, n, i);
        }

        // Heap sort
        for (int i = n - 1; i >= 0; i--) {
            int temp = arr[0];
            arr[0] = arr[i];
            arr[i] = temp;

            // Heapify root element
            heapify(arr, i, 0);
        }
    }

    static void heapify(int arr[], int n, int i) {
        // Find largest among root, left child and right child
        int largest = i;
        int l = 2 * i + 1;
        int r = 2 * i + 2;

        if (l < n && arr[l] > arr[largest])
            largest = l;

        if (r < n && arr[r] > arr[largest])
            largest = r;

        // Swap and continue heapifying if root is not largest
        if (largest != i) {
            int swap = arr[i];
            arr[i] = arr[largest];
            arr[largest] = swap;

            heapify(arr, n, largest);
        }
    }
}

class ShellSort {

    // Rearrange elements at each n/2, n/4, n/8, ... intervals
    static void shellSort(int array[], int n) {
        for (int interval = n / 2; interval > 0; interval /= 2) {
            for (int i = interval; i < n; i += 1) {
                int temp = array[i];
                int j;
                for (j = i; j >= interval && array[j - interval] > temp; j -= interval) {
                    array[j] = array[j - interval];
                }
                array[j] = temp;
            }
        }
    }
}